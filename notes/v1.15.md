# Please welcome /e/OS 1.15! :rocket:

We are proud to deliver the /e/OS 1.15. Enjoy all the new features and improvements it embeds!

📲 **App Lounge** allow you to ask for a privacy rating, when it's missing!

## ✨ We embedded some improvements!

👥 **Contacts** sync starred contact with Murena Cloud

📅 **Browser** autofill disabled by default

🌍 **Translations** in App Lounge

## 🕙 Software updates

📱 **Gigaset GS290** update to Android 12

📱 **Fairphone 4** firmware update with the August one (FP4.SP2K.B.089.20230807)

🗺️ **Maps** has been updated to MagicEarth v7.1.23.29.57BF7D98.E8C7767A

🔐 **Account manager** update with upstream (from [v4.3-ose](https://github.com/bitfireAT/davx5-ose/releases/tag/v4.3-ose) to [v4.3.5.2-ose](https://github.com/bitfireAT/davx5-ose/releases/tag/v4.3.5.2-ose))

➕ **microG** update with upstream ([v0.2.28.233013-135](https://github.com/microg/GmsCore/releases/tag/v0.2.28.231657))

➕ **LineageOS 19.1** last bug fixes and security updates has been merged ([list](https://review.lineageos.org/q/branch:lineage-19.1+status:merged+after:%222023-07-25+20:22:00+%252B0200%22+before:%222023-08-25+20:28:00+%252B0200%22))

## 🐛 Bug fixes

🆙 **Updater** now displays properly the available updates for all users

📲 **App Lounge** got time out and token expiration issues fixed

📱 **Samsung Galaxy S9 and S9+** GPS working as expected

📱 **Gigaset GS290** VoLTE and VoWiFi working as expected

📱 **Fairphone 4 Firmware** Bluetooth Qualcomm aptx HD audio now working properly

📱 **Murena One** shows the fingerprint sensor at the right place

📱 **Community devices** build fixed for some of them

🔄 **File Synchronization** notification shows the correct quota usage percentage

🗺️ **Maps** is able to open address links properly

📩 **Message** allows to share pictures

📶 **Network** default configuration allows to send messages via Mint Mobile

🚖 **Third-Party App support** sees some in-app maps fixes and performances improvements

📅 **Calendar** do not offer an about page, as it was causing calls to Google

📝 **Notes** Properly loads the Murena Cloud account avatar

🔏 **Advanced Privacy**
- *Fake location* feature properly turned off when the user disable the feature
- Maps now loading properly when switching between *Fake location* options

## ⚠ Security fixes

This /e/OS v1.15 includes the [Android security patches](https://source.android.com/security/bulletin/2023-08-01) available until August.

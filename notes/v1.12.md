# Please welcome /e/OS 1.12! :rocket:

We are proud to deliver the /e/OS 1.12. Enjoy all the new features and improvements it embeds!

## ✨ We embedded some improvements!

### General

* Fix battery drain issue on multiple devices
* Upgrade of Teracube 2e (2020) to Android 11
* Improved location accuracy

### App Lounge

* App lounge automatically detects the source (Gplay/Open Source) of installed app during updates

### Camera

* Pictures are now saved by default in a folder called Camera

### eDrive

* Update of Icelandic translation

### First Time Setup Wizard

* Turn off OEM unlocking during initial setup if bootloader is locked

## 🕙 Software updates

* We merged bug fixes and security updates from [LineageOS source code trees S](https://review.lineageos.org/q/branch:lineage-19.1+status:merged+after:%25222023-04-11+18:10:00+%252B0200%2522+before:%25222023-05-16+18:10:00+%252B0200%2522)
* We updated **microG** to version 0.2.28 with a new location & maps stack [changelog](https://github.com/microg/GmsCore/releases/tag/v0.2.28.231657)
* We updated **Mail** to version 6.703 [changelog](https://thundernest.github.io/k-9/changelog_main_branch.xml)

## 🐛 Bug fixes

### App Lounge

* Fixed automatic updates were interrupted when authentification was expired.
* Fixed a UI glitch displaying both "No updates available" and a list of apps to be updated.
* Fixed a regression in the Privacy Score calculation

### eDrive

* Fixed account switching issue

### Mail

* The alert dialog now follows our UI guidelines

### Gallery

* Fixed some crashes

### Advanced Privacy

* Fixed map loading while in the fake location entry
* Fixed crash happening in the background
* Fixed the way trackers are associated to an app
* Fixed adaptation to an orientation change while in an entry of the app

### Third party apps

* Fixed some bugs with some third party apps (Yuka for instance)

### Message

* Fixed some crash

## ⚠ Security fixes

This /e/OS v1.12 includes the [Android security patches](https://source.android.com/security/bulletin/2023-05-01) available until May.
